---
home: true
heroImage: /home_logo.png
heroText: 企业预警通移动开发文档
# tagline: Hero 副标题
actionText: 快速开始 →
actionLink: /guide/
features:
- title: Web 组件接口文档
  details: 原生和 Web 通信接口文档。
- title: 原生功能路由
  details: 原生功能模块路由跳转链接。
- title: 代码规范、代码提交规范、分支管理规范
  details: 日常开发规范。
footer: MIT Licensed | Copyright©Financial China Information & Technology Co., Ltd.All rights reserved.
---
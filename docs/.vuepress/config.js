module.exports = {
    title: '企业预警通移动开发文档',
    base: '/FCAPP-Docs/',
    description: '企业预警通移动开发文档',
    dest: 'public',
    themeConfig: {
        logo: '/logo.png',
        nav: [ 
            // 导航栏
            {
                text: '概述',
                link: '/'
            },
            {
                text: 'Web 组件',
                link: '/guide/web组件接口说明'
            },
            {
                text: '原生路由',
                link: '/guide/原生功能路由链接'
            }
        ],
        sidebar: 'auto',
        nextLinks: true,
        prevLinks: true
    }
}
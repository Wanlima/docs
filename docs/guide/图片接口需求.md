[toc]
# 图片接口需求

## 输入图片

* 图片名 name.png
* 图片大小  1600x1200
* 单位 像素

## 缩略图

1. 生成指定大小缩略图以及相关2倍,3倍图

2. 图片命名方式
    * name_width_height.png

3. 样例
    * 指定生成缩略图大小 80x60
    * 生成图片 name_80_60.png name_160_120.png name_240_180.png

4. 访问接口  
    https://cnd/name_80_60.png  
    https://cnd/name_160_120.png  
    https://cnd/name_240_180.png  

## 大图

1. 生成指定大小的浏览图片以及相关2倍,3倍图

2. 图片命名方式
    * name_width_height.png

3. 样例
    * 指定生成浏览图大小 480x360
    * 生成图片 name_480_360.png name_960_720.png name_1440_1080.png

4. 访问接口  
    https://cnd/name_480_360.png  

    https://cnd/name_960_720.png  

    https://cnd/name_1440_1080.png  

## 原图

* name.png

1. 访问接口  

  https://cnd/name.png

## 备注

如果不需要浏览图片，则不用生成大图.  
接口下发图片链接需要同时下发一倍图(最小图片)的大小，样例种的80x60  
app程序会根据屏幕分辨率，动态下载需要的图片.

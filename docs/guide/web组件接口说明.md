[toc]
# web组件说明

## 使用方法说明

1. iOS调用方法

    * 无返回值的函数  window.webkit.messageHandlers.function_name.postMessage(message)

    * 有返回值的函数  window.function_name(params)

2. Android调用方法
  
    * window.jsi.function_name(params)

备注:

1. function_name为对应的函数名

2. params为传参

## api

### void onAnchorShow(index)

#### 接口说明

页面通知APP锚点发生变化

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| index| int | true | 锚点索引 | 0 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注
1.android 通用web页面暂不支持

---

### void changeView(Bool)

#### 接口说明

向右旋转屏幕

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| isLandscapeRight| Bool | true | 是否横屏 | true |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void setTitle(String)

#### 接口说明

设置web组件标题

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| title | String | true | 标题 | {"title": "标题", "subTitle": "副标题"} |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void setStick(Bool)

#### 接口说明

通知app使web组件吸顶

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| isStick | Bool | true | 是否吸顶 | true |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void domReadyNotify()

#### 接口说明

通知app页面dom树渲染完毕

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void updateFCAccount(String)

#### 接口说明

更新app终端账号信息缓存

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| finInfo | String | true | 终端账号信息 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void updateLevelInfo(String)

#### 接口说明

```json
{
    "fin_user": "",
    "fin_type": "",
    "fin_enddt": "",
    "valid": 0
}
```

更新app用户等级信息缓存

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| levelInfo | String | true | 用户等级信息 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "lv": 4,
    "lv_type": "钻石VIP(dev)",
    "lv_title": "",
    "lv_enddt": "20210703000000",
    "sub_max": 20,
    "item_max": 500,
    "fin_sub_max": 20,
    "fin_item_max": 500
}
```

---

### void beginScreenshot()

#### 接口说明

页面通知app开始为截图准备数据

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void screenshot(Bool)

#### 接口说明

页面通知app截图数据准备完毕

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| canSnapshot | Bool | true | 是否可以截长图 | true |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void downloadCondition(String)

#### 接口说明

导出

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| param | String | true | 导出所需json字符串参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void needHideBackBtn(Bool)

#### 接口说明

显示/隐藏返回按钮

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| isHidde | Bool | true | 是否隐藏返回按钮 | false |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void Alert(String)
#### 接口说明

js唤起原生弹框（支持html格式内容作为弹框内容显示）

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| configData | String | true | 弹框json参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "title": "标题",
    "message":"支持纯字符串、html",
    "iconUrl": "弹框顶部图片",//权限系统弹框无图片链接时使用默认图片
    "showImage":true,
    "showClose":true,
    "textAlignment":"left",//center居中(默认); left左对齐; right右对齐
    "buttons": [
        {
            "title":"确定",
            "titleColor":"#FFFFFF",
            "action":"functionName"//可执行js方法
        }
    ]
}
```

------

### Alert1

---

### Alert2

---

### void request(String)

#### 接口说明

使用原生的http组件转发请求

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| requestData | String | true | 请求json参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "url": "",
    "params": {
        "key1": "value1",
        "key2": "value2"
    },
    "method": "POST",
    "callback": "callbacl"
}
```

---

### void upload(String)

#### 接口说明

使用原生的http组件上传文件

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| requestData | String | true | 请求json参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "url": "",
    "params": {
        "key1": "value1",
        "key2": "value2"
    },
    "filedata": "base64XXXX",//base64编码的文件
    "mimetype": "image/jpeg"
    "filename": "xxxxx.jpeg"
    "callback": "callbackJs"//上传完成后通知页面的js
}
```

---

### void forbidScroll(Bool)

#### 接口说明

打开或关闭F9详情页上推（下拉）动画效果

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| forbidScroll | Bool | true |  | true |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void isBounceEnable(Bool)

#### 接口说明

禁止web组件滚动的橡皮筋效果

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| isEnable | Bool | true | 是否开启橡皮筋效果 | true |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### void setTitleBar(String)

#### 接口说明

设置web组件标题栏效果

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| titleData | String | true | 标题栏json参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "showTitleBar": true,
    "statusBarHighLight":true,//状态栏是否高亮 (ture:安卓标题栏文字颜色为黑，苹果为白 false:安卓标题栏文字颜色为白，苹果为黑--------4.9版本以下)
    "statusBarShowWhite":true,//状态栏颜色（true:白色, false:黑色--------4.9版本以上支持）
    "backgroundColor": "#78828D",
    "title": {
        "text": "主标题",
        "color": "#78828D"
    },
    "subTitle": {
        "text": "副标题",
        "color": "#78828D"
    }
}
```

---

### void setLeftMenu(String)

#### 接口说明
设置web组件左边按钮

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| menuData | String | true | 菜单json参数 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
[{
    "linkUrl": "",  //优先判断该字段
    "callBack": "",  //如果linkUrl为空，则再判断该字段
    "imageUrl": "",  //按钮优先使用图片
    "text": "按钮文字",  //如果imageUrl为空，则使用文字
    "color": "#78828D"
}]
```

---

### void setRightMenu(String)

#### 接口说明

设置web组件右边按钮

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| menuData | String | 菜单json参数 |  | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
[
    {
        "linkUrl": "",
        "callBack": "",
        "imageUrl": "",
        "text": "按钮文字",
        "color": "#78828D",
        "guide":{
            "id": "XX1",//功能引导id，不同功能该字段不能相同。如果没有该字段，则【新功能引导】功能不会弹出
            "tips": "这是我们新增的“实时推送”功能，可支持实时接收推送相关内容，快来试试呀～",//功能引导提示文案
        },
        
        "menus": [{
            "imageUrl": "",
            "linkUrl": "",
            "text": "子菜单1",
            "color": "#78828D",
            "callBack": ""
        }]
    },
    {
        "linkUrl": "",
        "callBack": "",
        "imageUrl": "",
        "text": "按钮文字",
        "color": "#78828D",
         "guide":{
            "id": "XX2",//功能引导id，不同功能该字段不能相同。如果没有该字段，则【新功能引导】功能不会弹出
            "tips": "这是我们新增的“实时推送”功能，可支持实时接收推送相关内容，快来试试呀～",//功能引导提示文案
        },
    }
]
```

---

### void contentSizeChange(float)

#### 接口说明

返回当前页面内容高度

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| height | float | true | 页面内容高度 | 1080 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

### String getAppVersion()

#### 接口说明
获取app版本信息

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void |  |  | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| appVersion | String | |

#### 备注

---

### Array getWebShares()

#### 接口说明
获取app支持的分享类型

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| shares | Map | |

#### 备注

---

### String getAppType()
app版本类型，finchina, bjbank

#### 接口说明

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| appType | 版本类型 | |

#### 备注

---

### float getStatusBarHeight()

#### 接口说明
状态栏高度

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| statusBarHeight | float | |

#### 备注

---

### float getTitleBarHeight()

#### 接口说明
导航栏高度

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| titleBarHeight | float | |

#### 备注

---

### float getContentInsetTop()

#### 接口说明
iOS特有, scrollView top 偏移量

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | | | |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| contentInsetTop | float | |

#### 备注

---

## 原生调用h5页面js

### 导出

downloadCondition()

### 搜索

search(String)


### 关闭顶部菜单

closeMenu()
closeTopMenu()   （地区经济）

### 横竖屏幕

changeOrientation(Boolean)

### 唤起顶部菜单（地区经济）

switchTopMenu()

### IOS右滑判断

changeRangeStatus(Boolean)

### 目录树

setTreeData(Object)

### finishLoading

finishLoading(Boolean)

### 目录树设置高亮

setHighlight

### 地区经济状况跳转

toMemberPage （地区经济状况）

### 债券f9

gotoAntor  

### 图谱入口页面

inputText(String, number) 

---

### void beginScreenshot()

#### 接口说明

用于提示移动端显示加载圈

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

### 截长图完成

screenshotFinished（）

### 备注
#### 移动端 h5页面截长图功能流程如下(还可以参考文档：截长图时序图.drawio，帮助理解，地址为https://git.finchina.com/mobile-develop/fcapp-doc/blob/master/%E4%BC%81%E4%B8%9A%E9%A2%84%E8%AD%A6%E9%80%9A/%E6%88%AA%E9%95%BF%E5%9B%BE%E6%97%B6%E5%BA%8F%E5%9B%BE.drawio)
#### 1、移动端点击“存长图”按钮，调用前端接口screenshotStart()；
#### 2、前端立即调用移动端端接口beginScreenshot()，使Android端显示加载框提示；
#### 3、截止到前端页面接口数据请求完毕，前端再调用移动端接口screenshotPrepared（boolean flag）,flag = true,表示前端数据页面已准备好，移动端可以开始截长图；
#### 4、移动端截长图完毕后，调用前端接口screenshotFinished(),提示前端移动端截长图已完毕。
#### 注意点：h5页面，不能写死页面长宽，否则截图时，只显示一屏内容或者固定长图内容，其他空白的情况
####  Android客户端注意点：在设置布局之前添加if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {WebView.enableSlowWholeDocumentDraw();}，否则只能截取一屏，其他空白的情况，也可以webview外层添加scrollview控件，但是问题较多，不建议这种方法；
## 王小庆

---

### void userBehavior(String)

#### 接口说明

用户行为分析记录接口

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| data | String | true | 用户行为分析数据 | 见备注 |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

```json
{
    "event": "buttonClick",
    "values": {
        "title": "筛选",
        "from": "债券"
    }
}
```

values是字典转化的json字符串  
values属性关键字举例

1. title 标题
2. name 名称
3. url 链接
4. from 来源
5. duration 时长

目前原生使用的事件

1. beginLogModule 进入模块
2. endLogModule 退出模块
3. moduleMenuClick 点击模块功能菜单
4. buttonClick 点击事件
5. appEnter app启动/进入前台
6. appExit app退出/进入后台

---

### void closeDialog()

#### 接口说明

关闭原生alertWebView弹框

#### 请求参数说明

| 请求参数 | 类型 | 必填 | 参数说明 | 示例 |
| :--- | :--- | :--- | :--- | :--- |
| void | void | void | void | void |

#### 返回参数说明

| 返回参数 | 参数类型 | 参数说明 |
| :--- | :--- | :--- |
| void | void | 无返回参数 |

#### 备注

---

[toc]
# SwiftLint


### 介绍

SwiftLint 是 Realm 推出的一款 `Swift代码规范检查工具`。 

SwiftLint 基于 [Github 公布的 Swift 代码规范](https://github.com/github/swift-style-guide) 进行代码检查，并且能够很好的和 Xcode 整合。

SwiftLint 安装并配置好之后，在Xcode执行编译时，SwiftLint会自动运行检查，不符合规范的代码会通过警告或者error的形式指示出来，并且拥有丰富的配置项，可以进行大量的自定义，相当方便。


### 安装

统一使用Homebrew进行安装，首先要确保`源地址`统一，替换到清华源：

```
git -C "$(brew --repo)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git

# 以下针对 mac OS 系统上的 Homebrew
git -C "$(brew --repo homebrew/core)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git

git -C "$(brew --repo homebrew/cask)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-cask.git

git -C "$(brew --repo homebrew/cask-fonts)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-cask-fonts.git

git -C "$(brew --repo homebrew/cask-drivers)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-cask-drivers.git
```
执行安装命令：

```
brew install swiftlint
```
安装完毕，检查是否是最新版：

```
swiftlint version
```

查看swiftlint所有命令：

```
swiftlint help
```

### 配置Xcode


Xcode -> TARGETS -> Build Phases -> 新建 New Run Script Phase，添加脚本代码

```
# Type a script or drag a script file from your workspace to insert its path.
if which swiftlint >/dev/null; then
  cd ..
  swiftlint
else
  echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
fi

```


### 自定义配置


通过自定义配置来忽略第三方库的代码规范问题或者其他警告问题：在项目根目录下新建一个名为`.swiftlint.yml`的配置文件

主要的几个配置选项介绍：

```
 disabled_rules: # 执行时排除掉的规则，即：禁用指定的规则 
 
 opt_in_rules: # 一些规则仅仅是可选的，即：启用指定的规则
 
 included: # 执行 linting 时包含的路径。如果出现这个 `--path` 会被忽略。
 
 excluded: # 执行 linting 时忽略的路径。 优先级比 `included` 更高。
```

### 源码中的配置


可以提供在项目源文件中，定义一个如下格式的注释来关闭某个规则：

```
// swiftlint:disable <rule>
```
直到该文件结束之前，或者在`enable`之前，这条规则都会被禁用。

`enable`格式，打开某个规则：

```
// swiftlint:enable <rule>
```



参考：  
[SwiftLint-README_CN.md](https://github.com/realm/SwiftLint/blob/master/README_CN.md)  
[Homebrew清华源使用文档](https://mirrors.tuna.tsinghua.edu.cn/help/homebrew/)

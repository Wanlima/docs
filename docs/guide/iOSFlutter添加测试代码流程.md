# iOS-flutter运行测试代码流程
## ==注意事项：main.dart中enableFlutterDriverExtension()方法仅可在测试时添加，提交代码时需去除，否则会导致app键盘无法弹出==
## 操作流程
1. 下载fcfoundation（如已下载则忽略此步骤）
- cd 保存路径
- git clone https://git.finchina.com/mobile-develop/ios/fcfoundation
2. 编辑flutter测试代码并保存
3. 执行update_xcframeworks.sh打包flutter代码并输出到foundation/Binaries目录，操作如下:
- cd 项目所在文件夹路径/fcpublicopinionsystem_ios/fcnews_flutter
- ./update_xcframeworks.sh -b fcfoundation所在文件夹路径/fcfoundation/Binaries
4. 将fcfoundation拖入项目中